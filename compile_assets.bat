@SETLOCAL


IF NOT EXIST Data mkdir Data

pushd Data
IF EXIST Maps RMDIR /S /Q Maps
mkdir Maps
popd

pushd RawData
pushd Maps

FOR %%f IN (*.tmx) DO "../../Bin/TiledToLD34_Release.exe" %%f ../../Data/Maps/%%~nf.map

@REM Get out of Maps
popd
@REM Get out of RawData
popd


@ENDLOCAL
