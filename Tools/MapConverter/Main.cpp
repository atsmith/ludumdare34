// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#include <fstream>
#include <iostream>
#include "Tiled.h"
#include "LDMap.h"

void DisplayUsage( char* );
void DisplayError( char* );

int main( int argc, char** argv )
{
    if( 3 != argc )
    {
        DisplayUsage( argv[ 0 ] );
        return EXIT_FAILURE;
    }

    Tiled tiled_map_loader;

    if( !tiled_map_loader.Open( argv[ 1 ] ) )
    {
        DisplayError( "File does not exist!" );
        return EXIT_FAILURE;
    }

    TiledMap map = tiled_map_loader.GetMap( );

    if( !map.valid )
    {
        DisplayError( "Unable to load map, invalid file." );
        return EXIT_FAILURE;
    }

    LDMap ld_map;

    auto binary_output = ld_map.ToLDMapFormat( map );

    if( 0 == binary_output.size( ) )
    {
        DisplayError( "Failed to convert map." );
        return EXIT_FAILURE;
    }

    std::fstream output_file;
    output_file.open( argv[ 2 ], std::ios_base::binary | std::ios_base::out );
    if( !output_file.is_open( ) )
    {
        DisplayError( "Failed to create file." );
        return EXIT_FAILURE;
    }

    for( unsigned char byte : binary_output )
    {
        output_file << byte;
    }

    if( output_file.fail( ) )
    {
        DisplayError( "Something went wrong writing the file." );
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

void DisplayUsage( char* app_call )
{
    std::cout << "Usage: " << app_call << " <tiled_map.tmx> <output_name>" << std::endl;
}

void DisplayError( char* error_string )
{
    std::cout << "Error: " << error_string << std::endl;
}