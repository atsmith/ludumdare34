// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include <vector>
#include "Tiled.h"

class LDMap
{
public:
    LDMap( ) = default;
    std::vector<unsigned char> ToLDMapFormat( const TiledMap& map ) const;
};