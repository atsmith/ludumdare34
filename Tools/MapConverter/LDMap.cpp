// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#include <SerializationOPCodes.h>
#include "LDMap.h"

typedef unsigned char byte;

template<typename Type>
static void Serialize( std::vector<byte>& byte_array, Type value )
{
    byte* bytes = ( byte* )&value;
    for( size_t i = 0; i < sizeof( value ); i++ )
    {
        byte_array.push_back( bytes[ i ] );
    }
}

template<>
static void Serialize<const char*>( std::vector<byte>& byte_array, const char* value )
{
    unsigned long long length = strlen( value );
    Serialize( byte_array, length );
    for( size_t i = 0; i < length; i++ )
    {
        byte_array.push_back( value[ i ] );
    }
}

static void SerializeImage( std::vector<byte>& byte_array, const Image& image )
{
    Serialize( byte_array, image.source.c_str( ) );
    Serialize( byte_array, image.transparent_color );
    Serialize( byte_array, image.width );
    Serialize( byte_array, image.height );
}

static void SerializeLayerBasics( std::vector<byte>& byte_array, const Layer& layer )
{
    Serialize( byte_array, ( int )layer.layer_type );
    Serialize( byte_array, layer.name.c_str( ) );
    Serialize( byte_array, layer.opacity );
    Serialize( byte_array, layer.visible );
    Serialize( byte_array, layer.offset_x );
    Serialize( byte_array, layer.offset_y );
}

static void SerializeTileLayer( std::vector<byte>& byte_array, const TileLayer& layer )
{
    SerializeLayerBasics( byte_array, layer );
    unsigned long long tile_count = layer.tiles.size( );
    Serialize( byte_array, tile_count );
    for( Tile tile : layer.tiles )
    {
        Serialize( byte_array, tile.id );
    }
}

static void SerializeObjectLayer( std::vector<byte>& byte_array, const ObjectLayer& layer )
{
    SerializeLayerBasics( byte_array, layer );
    unsigned long long object_count = layer.objects.size( );
    Serialize( byte_array, object_count );
    for( ObjectLayer::Object object : layer.objects )
    {
        Serialize( byte_array, FromString( object.object_type ) );
        Serialize( byte_array, object.unique_id );
        Serialize( byte_array, object.name.c_str( ) );
        Serialize( byte_array, object.x );
        Serialize( byte_array, object.y );
        Serialize( byte_array, object.width );
        Serialize( byte_array, object.height );
        Serialize( byte_array, object.rotation );
        Serialize( byte_array, object.visible );
        Serialize( byte_array, !object.image.source.empty( ) );
        if( !object.image.source.empty( ) )
        {
            SerializeImage( byte_array, object.image );
        }
    }
}

std::vector<byte> LDMap::ToLDMapFormat( const TiledMap& map ) const
{
    static const std::vector<byte> empty_array;
    std::vector<byte> bytes;

    if( !map.valid )
    {
        return empty_array;
    }

    Serialize( bytes, map.width );
    Serialize( bytes, map.height );
    Serialize( bytes, map.tile_width );
    Serialize( bytes, map.tile_height );
    Serialize( bytes, map.background_color );
    unsigned int tileset_count = ( unsigned int )map.tilesets.size( );
    unsigned int layer_count = ( unsigned int )map.layers.size( );
    Serialize( bytes, tileset_count );
    Serialize( bytes, layer_count );
    for( Tileset tileset : map.tilesets )
    {
        Serialize( bytes, tileset.first_global_id );
        Serialize( bytes, tileset.name.c_str( ) );
        Serialize( bytes, tileset.tile_width );
        Serialize( bytes, tileset.tile_height );
        Serialize( bytes, tileset.spacing );
        Serialize( bytes, tileset.margin );
        Serialize( bytes, tileset.tile_count );
        SerializeImage( bytes, tileset.image );
        Serialize( bytes, tileset.tile_offset.x );
        Serialize( bytes, tileset.tile_offset.y );
        for( Tile tile : tileset.tiles )
        {
            Serialize( bytes, tile.id );
        }
    }

    for( std::shared_ptr<Layer> layer : map.layers )
    {
        switch( layer->layer_type )
        {
        case LayerType::TileLayer:
            SerializeTileLayer( bytes, *reinterpret_cast< TileLayer* >( layer.get( ) ) );
            break;
        case LayerType::ObjectLayer:
            SerializeObjectLayer( bytes, *reinterpret_cast< ObjectLayer* >( layer.get( ) ) );
            break;
        default:
            return empty_array;
        }
    }
    return bytes;
}