// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include <memory>
#include <string>
#include <vector>
#include <tinyxml/tinyxml2.h>

struct Image
{
    std::string source;
    unsigned int transparent_color;
    unsigned int width;
    unsigned int height;
};

struct Tile
{
    unsigned int id;
};

struct Tileset
{

    struct TileOffset
    {
        int x;
        int y;
    };

    unsigned int first_global_id;
    std::string name;
    int tile_width;
    int tile_height;
    int spacing;
    int margin;
    unsigned int tile_count;
    Image image;
    TileOffset tile_offset;
    std::vector<Tile> tiles;

};

enum class LayerType : unsigned char
{
    TileLayer = 0,
    ObjectLayer,
    ImageLayer,
    None
};

struct Layer
{
    std::string name;
    float opacity;
    bool visible;
    int offset_x;
    int offset_y;
    LayerType layer_type;
};

struct TileLayer : Layer
{
    std::vector<Tile> tiles{ };
};

struct ObjectLayer : Layer
{
    struct Object
    {
        unsigned int unique_id;
        std::string name;
        std::string object_type;
        int x;
        int y;
        int width;
        int height;
        float rotation;
        bool visible;
        Image image;
    };

    unsigned int group_color;
    std::vector<Object> objects;
};

struct TiledMap
{
    bool valid;
    int width;
    int height;
    int tile_width;
    int tile_height;
    unsigned int background_color;
    std::vector<Tileset> tilesets;
    std::vector<std::shared_ptr<Layer>> layers;
};

class Tiled
{
public:
    Tiled( ) = default;
    bool Open( const std::string& map_name );
    TiledMap GetMap( ) const;
private:
    tinyxml2::XMLDocument document;
};