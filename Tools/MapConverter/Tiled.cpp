// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#include <map>
#include "Tiled.h"

static const std::map<char, int> hex_map = {
    { '0', 0 }, { '1', 1 }, { '2', 2 }, { '3', 3 }, { '4', 4 },
    { '5', 5 }, { '6', 6 }, { '7', 7 }, { '8', 8 }, { '9', 9 },
    { 'A', 10 }, { 'a', 10 }, { 'B', 11 }, { 'b', 11 }, { 'C', 12 }, { 'c', 12 },
    { 'D', 13 }, { 'd', 13 }, { 'E', 14 }, { 'e', 14 }, { 'F', 15 }, { 'f', 15 } };

static int Hex2Int( const char hex )
{
    auto found = hex_map.find( hex );
    if( hex_map.end( ) == found )
    {
        //TODO: Bad character entered for hex -- log this
        return 0;
    }

    return found->second;
}

static unsigned int Hex2RGB( const std::string& hex )
{
    static unsigned int alpha_mask = 0x000000FF;
    static unsigned int red_mask = 0xFF000000;
    static unsigned int green_mask = 0x00FF0000;
    static unsigned int blue_mask = 0x0000FF00;
    if( hex[ 0 ] != '#' && hex[0] != '0' && Hex2Int(hex[0]) == 0 )
    {
        return 0xFFFFFFFF;
    }

    size_t pos_alpha_start = 9;

    if( hex[ 0 ] != '#' )
    {
        pos_alpha_start = 8;
    }

    unsigned int color = 0x00000000;
    size_t pos = hex.length( ) - 1;

    if( hex.length( ) == pos_alpha_start )
    {
        // load the alpha color if it was provided
        color = Hex2Int( hex[ pos-- ] ) * 16 + Hex2Int( hex[ pos ] );
        pos--;
    }
    else
    {
        color = 0x000000FF;
    }
    unsigned char blue = Hex2Int( hex[ pos-- ] ) + Hex2Int( hex[ pos-- ] ) * 16;
    unsigned char green = Hex2Int( hex[ pos-- ] ) + Hex2Int( hex[ pos-- ] ) * 16;
    unsigned char red = Hex2Int( hex[ pos-- ] ) + Hex2Int( hex[ pos ] ) * 16;

    color |= ( color & red_mask ) | ( red << 24 );
    color |= ( color & green_mask ) | ( green << 16 );
    color |= ( color & blue_mask ) | ( blue << 8 );

    return color;

}

bool Tiled::Open( const std::string& map_name )
{
    if( tinyxml2::XML_NO_ERROR != document.LoadFile( map_name.c_str( ) ) )
    {
        return false;
    }
    tinyxml2::XMLElement* map_node = document.RootElement( );
    if( !map_node )
    {
        return false;
    }
    std::string map_node_name = map_node->Name( );
    if( map_node_name != "map" )
    {
        return false;
    }
    return true;
}

#define XML_QUERY(query) \
    if(tinyxml2::XML_NO_ERROR != query)\
    {\
        goto error;\
    }

#define XML_QUERY_OPT(pointer, query, default_value) \
    if(tinyxml2::XML_NO_ERROR != query)\
    {\
        *pointer = default_value;\
    }

#define QUERY_INT(element, name, pointer) XML_QUERY(element->QueryIntAttribute(name, pointer))
#define QUERY_UINT(element, name, pointer) XML_QUERY(element->QueryUnsignedAttribute(name, pointer))
#define QUERY_FLOAT(element, name, pointer) XML_QUERY(element->QueryFloatAttribute(name, pointer))
#define QUERY_BOOL(element, name, pointer) XML_QUERY(element->QueryBoolAttribute(name, pointer))



#define QUERY_INT_OPT(element, name, pointer, default_value) XML_QUERY_OPT(pointer, element->QueryIntAttribute(name, pointer), default_value)
#define QUERY_UINT_OPT(element, name, pointer, default_value) XML_QUERY_OPT(pointer, element->QueryUnsignedAttribute(name, pointer), default_value)
#define QUERY_FLOAT_OPT(element, name, pointer, default_value) XML_QUERY_OPT(pointer, element->QueryFloatAttribute(name, pointer), default_value)
#define QUERY_BOOL_OPT(element, name, pointer, default_value) XML_QUERY_OPT(pointer, element->QueryBoolAttribute(name, pointer), default_value)

static Image LoadImageElement( const tinyxml2::XMLElement* image_element )
{
    std::string transparent_color{ };
    Image image;
    image.source = image_element->Attribute( "source" );
    if( image.source.empty( ) )
    {
        // can't have an empty source!
        goto error;
    }
    size_t pos = image.source.find_last_of( "/" );
    if( std::string::npos == pos )
    {
        pos = image.source.find_last_of( "\\" );
    }
    if( std::string::npos != pos )
    {
        image.source = image.source.substr( pos + 1, image.source.length( ) - pos );
    }
    transparent_color = image_element->Attribute( "trans" );
    if( !transparent_color.empty( ) )
    {
        image.transparent_color = Hex2RGB( transparent_color );
    }
    else
    {
        image.transparent_color = 0xFFFFFFFF;
    }
    QUERY_UINT( image_element, "width", &image.width );
    QUERY_UINT( image_element, "height", &image.height );

    return image;
error:
    return Image{ };
}

static std::shared_ptr<Layer> LoadTileLayer( const tinyxml2::XMLElement* element )
{
    std::shared_ptr<TileLayer> layer{ new TileLayer };
    if( !layer )
    {
        return layer;
    }
    // set this in case of error
    layer->layer_type = LayerType::None;
    layer->name = element->Attribute( "name" );
    QUERY_FLOAT_OPT( element, "opacity", &layer->opacity, 1.f );
    QUERY_BOOL_OPT( element, "visible", &layer->visible, true );
    QUERY_INT_OPT( element, "offsetx", &layer->offset_x, 0 );
    QUERY_INT_OPT( element, "offsety", &layer->offset_y, 0 );

    const tinyxml2::XMLElement* data_element = element->FirstChildElement( "data" );

    if( data_element )
    {
        const tinyxml2::XMLElement* tile_element = data_element->FirstChildElement( "tile" );
        for( ; tile_element; )
        {
            Tile tile;
            QUERY_UINT( tile_element, "gid", &tile.id );
            layer->tiles.push_back( tile );
            tile_element = tile_element->NextSiblingElement( "tile" );
        }
    }

    layer->layer_type = LayerType::TileLayer;
    // fall through
error:
    return layer;
}

static std::shared_ptr<Layer> LoadObjectLayer( const tinyxml2::XMLElement* element )
{
    std::shared_ptr<ObjectLayer> layer{ new ObjectLayer };
    if( !layer )
    {
        return layer;
    }
    // set this in case of error
    layer->layer_type = LayerType::None;
    layer->name = element->Attribute( "name" );
    QUERY_UINT_OPT( element, "color", &layer->group_color, 0xFFFFFFFF );
    QUERY_FLOAT_OPT( element, "opacity", &layer->opacity, 1.f );
    QUERY_BOOL_OPT( element, "visible", &layer->visible, true );
    QUERY_INT_OPT( element, "offsetx", &layer->offset_x, 0 );
    QUERY_INT_OPT( element, "offsety", &layer->offset_y, 0 );

    const tinyxml2::XMLElement* object_element = element->FirstChildElement( "object" );

    for( ; object_element; )
    {
        ObjectLayer::Object object;

        QUERY_UINT( object_element, "id", &object.unique_id );
        object.name = object_element->Attribute( "name" );
        if( object.name.empty( ) )
        {
            goto error;
        }
        object.object_type = object_element->Attribute( "type" );
        if( object.object_type.empty( ) )
        {
            goto error;
        }
        QUERY_INT( object_element, "x", &object.x );
        QUERY_INT( object_element, "y", &object.y );
        QUERY_INT_OPT( object_element, "width", &object.width, 0 );
        QUERY_INT_OPT( object_element, "height", &object.height, 0 );
        QUERY_FLOAT_OPT( object_element, "rotation", &object.rotation, 0.f );
        QUERY_BOOL_OPT( object_element, "visible", &object.visible, true );

        const tinyxml2::XMLElement* image_element = object_element->FirstChildElement( "image" );
        if( image_element )
        {
            object.image = LoadImageElement( image_element );
            if( object.image.source.empty( ) )
            {
                goto error;
            }
        }

        layer->objects.push_back( object );
        object_element = object_element->NextSiblingElement( "object" );
    }

    layer->layer_type = LayerType::ObjectLayer;
    // fall through
error:
    return layer;
}

static std::shared_ptr<Layer> LoadLayer( const tinyxml2::XMLElement* element )
{
    std::string layer_name = element->Name( );
    if( layer_name == "layer" )
    {
        return LoadTileLayer( element );
    }
    else if( layer_name == "objectgroup" )
    {
        return LoadObjectLayer( element );
    }

    return std::shared_ptr<Layer>{};
}

TiledMap Tiled::GetMap( ) const
{
    std::string background_color{ };
    TiledMap map;
    // in case of error
    map.valid = false;
    // initialize background to white
    map.background_color = 0xFFFFFFFF;
    const tinyxml2::XMLElement* element = document.RootElement( );
    if( tinyxml2::XML_NO_ERROR != element->QueryIntAttribute( "width", &map.width ) )
    {
        // something went wrong, but I don't have time to deal with it
        goto error;
    }
    QUERY_INT( element, "height", &map.height );
    QUERY_INT( element, "tilewidth", &map.tile_width );
    QUERY_INT( element, "tileheight", &map.tile_height );
    background_color = element->Attribute( "backgroundcolor" );
    if( !background_color.empty( ) )
    {
        map.background_color = Hex2RGB( background_color );
    }

    element = element->FirstChildElement( "tileset" );

    for( ; 0 == strcmp(element->Name(), "tileset"); )
    {
        Tileset tileset;
        QUERY_UINT( element, "firstgid", &tileset.first_global_id );
        tileset.name = element->Attribute( "name" );
        if( tileset.name.empty( ) )
        {
            goto error;
        }
        QUERY_INT( element, "tilewidth", &tileset.tile_width );
        QUERY_INT( element, "tileheight", &tileset.tile_height );
        QUERY_INT_OPT( element, "spacing", &tileset.spacing, 0 );
        QUERY_INT_OPT( element, "margin", &tileset.margin, 0 );
        QUERY_UINT( element, "tilecount", &tileset.tile_count );
        const tinyxml2::XMLElement* tile_offset_element = element->FirstChildElement( "tileoffset" );
        if( tile_offset_element )
        {
            QUERY_INT( tile_offset_element, "x", &tileset.tile_offset.x );
            QUERY_INT( tile_offset_element, "y", &tileset.tile_offset.y );
        }
        else
        {
            tileset.tile_offset.x = tileset.tile_offset.y = 0;
        }
        const tinyxml2::XMLElement* tile_element = element->FirstChildElement("tile");
        for( ; tile_element; )
        {
            Tile tile;
            QUERY_UINT( tile_element, "id", &tile.id );
            tileset.tiles.push_back( tile );
        }
        const tinyxml2::XMLElement* image_element = element->FirstChildElement( "image" );
        if( image_element )
        {
            tileset.image = LoadImageElement( image_element );
            if( tileset.image.source.empty( ) )
            {
                goto error;
            }
        }
        map.tilesets.push_back( tileset );
        element = element->NextSiblingElement( );
    }

    // no need, already been done by the previous for-loop
    //element = element->NextSiblingElement( );
    for( ; element; )
    {
        std::shared_ptr<Layer> layer = LoadLayer( element );
        if( !layer )
        {
            goto error;
        }
        map.layers.push_back( layer );
        element = element->NextSiblingElement( );
    }

    map.valid = true;
    // fall through, as this is where we want to be
error:
    return map;
}