// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#include <Box2D/Collision/Shapes/b2PolygonShape.h>
#include <Box2D/Dynamics/b2Fixture.h>
#include <SFML/Window/Mouse.hpp>
#include "Player.h"
#include "Globals.h"

GLOBAL_VAR Player player;

Player* Player::GetPlayerPtr( )
{
    return &player;
}

Player::Player( )
{
    if( texture_controller.IsResourceLoaded("Player.png") || texture_controller.LoadResource( "Player.png" ) )
    {
        SetSprite( *texture_controller.GetResourcePtr( "Player.png" ) );
    }
}

void Player::SetRequiredFood( int required_food )
{
    babyFoodNeedToGather = required_food;
}

void Player::OnMouseMove( const sf::Vector2f& new_position )
{
    targetPosition = new_position;
}

void Player::OnMouseUp( sf::Mouse::Button button_up )
{

}

void Player::Step( float delta_time )
{
    if( !hasCollider )
    {
        CreateCollider( );
    }
    b2Body* collider = GetCollider( );
    b2Vec2 collider_position = collider->GetPosition( );
    position = sf::Vector2f{ collider_position.x, collider_position.y };
    sf::Vector2f direction = targetPosition - position( );
    direction *= delta_time * moveSpeed;

    collider->SetLinearVelocity( b2Vec2{ direction.x, direction.y } );

    Actor::Step( delta_time );
}

void Player::GatherFood( )
{

}

void Player::Reset( )
{
    AttachCollider( nullptr );
    hasCollider = false;
    holdingBaby = false;
    babyFoodGathered = 0;
    babyFoodNeedToGather = 0;
}

void Player::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    sf::Vector2f sprite_position{ };
    if( hasCollider )
    {
        b2Body* collider = GetCollider( );
        b2Vec2 col_center = collider->GetWorldCenter( );
        sprite_position.x = col_center.x - boundingBox( ).width / 2;
        sprite_position.y = col_center.y - boundingBox( ).height / 2;
    }
    else
    {
        sprite_position = position( );
    }
    sf::Sprite sprite = GetSprite( );
    sprite.setPosition( sprite_position );
    target.draw( sprite, states );
}

void Player::CreateCollider( )
{
    b2BodyDef body_definition;
    body_definition.type = b2_dynamicBody;
    body_definition.position.Set( position( ).x, position( ).y );
    b2Body* body = Map::GetWorld( )->CreateBody( &body_definition );

    b2PolygonShape bounding_box_shape;
    bounding_box_shape.SetAsBox( boundingBox( ).width / 2, boundingBox( ).height / 2 );
    
    b2FixtureDef fixture_definition;
    fixture_definition.shape = &bounding_box_shape;
    fixture_definition.density = 1.0f;
    fixture_definition.friction = 0.3f;

    body->CreateFixture( &fixture_definition );

    AttachCollider( body );

    hasCollider = true;
}