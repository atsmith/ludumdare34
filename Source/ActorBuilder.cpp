// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#include "ActorBuilder.h"
#include "Wall.h"
#include "Tile.h"
#include "PlayerSpawn.h"

std::shared_ptr<Actor> ActorBuilder::Create( ActorOPCodes code )
{
    std::shared_ptr<Actor> actor;
    switch( code )
    {
    case ActorOPCodes::Wall:
        actor = std::shared_ptr<Actor>{ new Wall };
        break;
    case ActorOPCodes::Tile:
        actor = std::shared_ptr<Actor>{ new Tile };
        break;
    case ActorOPCodes::Actor:
        actor = std::shared_ptr<Actor>{ new Actor };
        break;
    case ActorOPCodes::PlayerSpawn:
        actor = std::shared_ptr<Actor>{ new PlayerSpawn };
        break;
    case ActorOPCodes::Unknown:
    default:
        break;
    }

    return actor;
}

std::shared_ptr<Actor> ActorBuilder::Create( ActorOPCodes code, const sf::Vector2f& position, const sf::Vector2f& scale, float rotation )
{
    std::shared_ptr<Actor> actor = Create( code );
    if( !actor )
    {
        return std::shared_ptr<Actor>{};
    }
    actor->position = position;
    if( ActorOPCodes::Wall == code )
    {
        auto wall_actor = std::static_pointer_cast< Wall >( actor );
        wall_actor->size = scale;
    }
    else
    {
        actor->scale = scale;
    }
    actor->rotation = rotation;

    return actor;
}

std::shared_ptr<Actor> ActorBuilder::Create( ActorOPCodes code, const std::shared_ptr<Actor>& pointer )
{
    std::shared_ptr<Actor> actor;
    switch( code )
    {
    case ActorOPCodes::Wall:
        actor = std::shared_ptr<Actor>{ new Wall{ *( Wall* )pointer.get( ) } };
        break;
    case ActorOPCodes::Tile:
        actor = std::shared_ptr<Actor>{ new Tile{ *( Tile* )pointer.get( ) } };
        break;
    case ActorOPCodes::Actor:
        actor = std::shared_ptr<Actor>{ new Actor{ *pointer } };
        break;
    case ActorOPCodes::PlayerSpawn:
        actor = std::shared_ptr<Actor>{ new PlayerSpawn{ *( PlayerSpawn* )pointer.get( ) } };
        break;
    case ActorOPCodes::Unknown:
    default:
        break;
    }

    return actor;
}