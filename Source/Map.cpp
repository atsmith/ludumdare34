// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#include "Map.h"
#include <SFML/System/FileInputStream.hpp>
#include "ActorBuilder.h"
#include "SerializationOPCodes.h"
#include "Globals.h"
#include "ColorTools.h"

typedef errno_t(* volatile memcpy_volatile_type )( void*, size_t, const void*, size_t );
GLOBAL_VAR memcpy_volatile_type memcpy_volatile{ memcpy_s };

struct MapHeader
{
    int width;
    int height;
    int tile_width;
    int tile_height;
    unsigned int background_color;
    unsigned int tileset_count;
    unsigned int layer_count;
};

template<typename CopyTo>
static void CopyData( std::vector<sf::Uint8>& byte_array, CopyTo* copy_to )
{
    memcpy_volatile( copy_to, sizeof( CopyTo ), &byte_array[ 0 ], sizeof( CopyTo ) );
    std::vector<sf::Uint8>( byte_array.begin( ) + sizeof( CopyTo ), byte_array.end( ) ).swap( byte_array );
}

static void CopyData( std::vector<sf::Uint8>& byte_array, char* copy_to, size_t mem_size )
{
    memcpy_volatile( copy_to, mem_size, &byte_array[ 0 ], mem_size );
    std::vector<sf::Uint8>( byte_array.begin( ) + mem_size, byte_array.end( ) ).swap( byte_array );
}

static bool DeserializeImage( std::vector<sf::Uint8>& byte_array, std::shared_ptr<sf::Texture>& image )
{
    unsigned long long image_name_length;
    CopyData( byte_array, &image_name_length );
    std::string image_name;
    image_name.resize( image_name_length );
    CopyData( byte_array, &image_name[ 0 ], image_name_length );
    unsigned int image_transparent_color;
    unsigned int width, height;
    CopyData( byte_array, &image_transparent_color );
    CopyData( byte_array, &width );
    CopyData( byte_array, &height );
    sf::Color transparent_color = ColorFromUInt( image_transparent_color );
    if( texture_controller.IsResourceLoaded( image_name ) )
    {
        image = texture_controller.GetResourcePtr( image_name );
        return true;
    }
    if( image_controller.IsResourceLoaded( image_name ) || image_controller.LoadResource( image_name ) )
    {
        sf::Image loaded_image = image_controller.GetResource( image_name );
        loaded_image.createMaskFromColor( transparent_color );
        if( !texture_controller.LoadResource( image_name, loaded_image ) )
        {
            return false;
        }

        image = texture_controller.GetResourcePtr( image_name );
        return true;
    }

    return false;
}

static void DeserializeLayerBasics( std::vector<sf::Uint8>& byte_array, std::shared_ptr<Layer>& layer )
{
    unsigned long long layer_name_length;
    CopyData( byte_array, &layer_name_length );
    std::string layer_name;
    layer_name.resize( layer_name_length );
    CopyData( byte_array, &layer_name[ 0 ], layer_name_length );
    float layer_opacity;
    CopyData( byte_array, &layer_opacity );
    bool visible;
    CopyData( byte_array, &visible );
    layer->SetOpacity( layer_opacity );
    layer->SetVisible( visible );
    int offset_x, offset_y;
    CopyData( byte_array, &offset_x );
    CopyData( byte_array, &offset_y );
}

static bool DeserializeTileLayer( std::vector<sf::Uint8>& byte_array, std::shared_ptr<Layer>& layer, const std::map<unsigned int, std::pair<std::shared_ptr<sf::Texture>, sf::IntRect>>& tiles, const sf::Vector2i& tile_size )
{
    DeserializeLayerBasics( byte_array, layer );
    unsigned long long tile_count;
    CopyData( byte_array, &tile_count );
    sf::FloatRect layer_size = layer->GetBoundingBox( );
    int row_count = ( int )( layer_size.width / tile_size.x );
    int column_count = ( int )( layer_size.height / tile_size.y );
    int current_row = 0;
    for( unsigned long long i = 0; i < tile_count; i++ )
    {
        if( 0 != i && 0 == ( i % column_count ) )
        {
            current_row++;
        }
        std::shared_ptr<Actor> tile = ActorBuilder::Create( ActorOPCodes::Tile );
        if( !tile )
        {
            return false;
        }
        unsigned int tile_id;
        CopyData( byte_array, &tile_id );
        if( 0 == tile_id || 2147483650 == tile_id )
        {
            continue;
        }
        auto& texture_pair = tiles.at( tile_id );
        tile->SetSprite( *texture_pair.first );
        tile->SetSpriteRect( texture_pair.second );
        sf::Vector2f tile_position;
        tile_position.x = ( float )( ( i % column_count ) * tile_size.x );
        tile_position.y = ( float )( current_row * tile_size.y );
        tile->position = tile_position;

        if( !layer->AddActor( tile ) )
        {
            return false;
        }
    }

    return true;
}

static bool DeserializeObjectLayer( std::vector<sf::Uint8>& byte_array, std::shared_ptr<Layer>& layer, const sf::Vector2i tile_size )
{
    DeserializeLayerBasics( byte_array, layer );
    auto object_layer = std::static_pointer_cast< ActiveLayer >( layer );
    unsigned long long object_count;
    CopyData( byte_array, &object_count );
    for( unsigned long i = 0; i < object_count; i++ )
    {
        ActorOPCodes object_type;
        CopyData( byte_array, &object_type );
        unsigned int unique_id;
        CopyData( byte_array, &unique_id );
        unsigned long long object_name_length;
        CopyData( byte_array, &object_name_length );
        std::string object_name;
        object_name.resize( object_name_length );
        CopyData( byte_array, &object_name[ 0 ], object_name_length );
        int x, y, width, height;
        float rotation;
        bool visible, has_image;
        std::shared_ptr<sf::Texture> texture;
        CopyData( byte_array, &x );
        CopyData( byte_array, &y );
        CopyData( byte_array, &width );
        CopyData( byte_array, &height );
        CopyData( byte_array, &rotation );
        CopyData( byte_array, &visible );
        CopyData( byte_array, &has_image );
        if( has_image && !DeserializeImage( byte_array, texture ) )
        {
            return false;
        }

        sf::Vector2f position = sf::Vector2f{ (float)x, (float)y };
        sf::Vector2f scale{ (float)width, (float)height };
        if( has_image )
        {
            sf::Vector2u image_size = texture->getSize( );
            float image_width = ( float )image_size.x;
            float image_height = ( float )image_size.y;
            float scale_width = ( float )width / image_width;
            float scale_height = ( float )height / image_height;
            scale = sf::Vector2f{ scale_width, scale_height };
        }
        std::shared_ptr<Actor> actor = ActorBuilder::Create( object_type, position, scale, rotation );

        if( !actor )
        {
            return false;
        }

        if( !object_layer->AddActor( actor ) )
        {
            return false;
        }
    }
    return true;
}

GLOBAL_VAR Map* world{ nullptr };

Map* Map::GetWorld( )
{
    return world;
}

Map::Map( )
    : background{ this, &Map::GetBackgroundColor },
    physicsWorld{ b2Vec2{ 0.f, 0.f } }
{

}

Map::Map( const Map& other )
    : Map{ }
{
    Clone( other );
}

Map& Map::operator=( const Map& other )
{
    Clone( other );
    return *this;
}

bool Map::loadFromFile( const sf::String& file_name )
{
    sf::FileInputStream file_input_stream;
    if( !file_input_stream.open( file_name ) )
    {
        return false;
    }

    sf::Int64 file_size = file_input_stream.getSize( );
    std::vector<sf::Uint8> byte_array;
    byte_array.resize( file_size );

    sf::Int64 bytes_read = file_input_stream.read( &byte_array[ 0 ], file_size );

    if( -1 == bytes_read )
    {
        return false;
    }

    MapHeader map_header;
    CopyData( byte_array, &map_header );
    
    mapSize.x = map_header.width * map_header.tile_width;
    mapSize.y = map_header.height * map_header.tile_height;
    tileSize.x = map_header.tile_width;
    tileSize.y = map_header.tile_height;
    backgroundColor.a = map_header.background_color & alpha_mask;
    backgroundColor.b = ( map_header.background_color & blue_mask ) >> 8;
    backgroundColor.g = ( map_header.background_color & green_mask ) >> 16;
    backgroundColor.r = ( map_header.background_color & red_mask ) >> 24;

    std::map<unsigned int, std::pair<std::shared_ptr<sf::Texture>, sf::IntRect>> tiles;

    for( unsigned int i = 0; i < map_header.tileset_count; i++ )
    {
        unsigned int global_id;
        CopyData( byte_array, &global_id );
        unsigned long long name_length;
        CopyData( byte_array, &name_length );
        std::string name;
        name.resize( name_length );
        CopyData( byte_array, &name[ 0 ], name_length );
        struct TilesetData
        {
            int tile_width;
            int tile_height;
            int spacing;
            int margin;
            unsigned int tile_count;
        };

        TilesetData tileset_data;
        CopyData( byte_array, &tileset_data );
        std::shared_ptr<sf::Texture> texture;
        if( !DeserializeImage( byte_array, texture ) )
        {
            return false;
        }

        int offset_x, offset_y;
        CopyData( byte_array, &offset_x );
        CopyData( byte_array, &offset_y );

        int tile_columns = texture->getSize( ).x / tileset_data.tile_width;
        int tile_rows = texture->getSize( ).y / tileset_data.tile_height;

        for( unsigned int j = 0, current_row = 0; j < tileset_data.tile_count; j++ )
        {
            sf::IntRect tile_location{ tileset_data.margin, tileset_data.margin, tileset_data.tile_width, tileset_data.tile_height };
            if( tile_rows != current_row && 0 != current_row )
            {
                tile_location.top += tileset_data.spacing;
            }
            if( 0 != ( j % tile_columns ) )
            {
                tile_location.left += tileset_data.spacing;
            }
            tile_location.top += current_row * tileset_data.tile_height;
            tile_location.left += (j % tile_columns) * tileset_data.tile_width;

            auto success = tiles.insert( std::make_pair( global_id + j, std::make_pair( texture, tile_location ) ) );

            if( !success.second )
            {
                return false;
            }

            // if we are on the last tile in the row, increment
            if( 0 == ( ( j + 1 ) % tile_columns ) )
            {
                current_row++;
            }
        }
    }

    sf::FloatRect layer_bounding_box{ 0.f, 0.f, (float)mapSize.x, (float)mapSize.y };
    for( unsigned int i = 0; i < map_header.layer_count; i++ )
    {
        LayerOPCodes layer_type;
        CopyData( byte_array, (int*)&layer_type );
        std::shared_ptr<Layer> layer;
        switch( layer_type )
        {
        case LayerOPCodes::TileLayer:
        {
            layer = std::shared_ptr<Layer>{ new Layer{ layer_bounding_box } };
            if( !layer )
            {
                return false;
            }
            if( !DeserializeTileLayer( byte_array, layer, tiles, tileSize ) )
            {
                return false;
            }
            break;
        }
        case LayerOPCodes::ObjectLayer:
        {
            layer = std::shared_ptr<Layer>{new ActiveLayer{ layer_bounding_box }};
            if( !layer )
            {
                return false;
            }
            if( !DeserializeObjectLayer( byte_array, layer, tileSize ) )
            {
                return false;
            }
            break;
        }
        default:
            return false;
        }
        layers.push_back( layer );
    }

    return true;
}

void Map::Step( float delta_time )
{
    if( this != world )
    {
        world = this;
    }

    physicsWorld.Step( delta_time, 20, 10 );

    if( player )
    {
        player->Step( delta_time );
    }
    for( std::shared_ptr<Layer> layer : layers )
    {
        layer->Step( delta_time );
    }
}

void Map::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    for( std::shared_ptr<Layer> layer : layers )
    {
        target.draw( *layer, states );
    }

    if( player )
    {
        target.draw( *player, states );
    }
}

void Map::Clone( const Map& other )
{
    mapSize = other.mapSize;
    tileSize = other.tileSize;
    backgroundColor = other.background;
    for( auto other_layer : other.layers )
    {
        std::shared_ptr<Layer> layer = other_layer->Clone( );
        layers.push_back( layer );
    }
}

sf::Color Map::GetBackgroundColor( ) const
{
    return backgroundColor;
}

void Map::SetPlayer( Player* player )
{
    this->player = player;
}

b2Body* Map::CreateBody( b2BodyDef* def )
{
    return physicsWorld.CreateBody( def );
}