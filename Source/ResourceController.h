// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include <memory>
#include <map>
#include <SFML/System/String.hpp>
#include <SFML/Graphics/Image.hpp>
#include <SFML/Graphics/Texture.hpp>
#include "Config.h"

GLOBAL_VAR const sf::String directory_separator{ "/" };

template<typename ResourceType>
class ResourceController
{
public:
    ResourceController( const sf::String& data_location );
    bool IsResourceLoaded( const sf::String& resource_name ) const;
    bool LoadResource( const sf::String& resource_name );
    template<typename OtherResource>
    bool LoadResource( const sf::String& resource_name, const OtherResource& resource );
    std::shared_ptr<ResourceType> GetResourcePtr( const sf::String& resource_name );
    ResourceType& GetResource( const sf::String& resource_name );
private:
    sf::String dataLocation;
    std::map<sf::String, std::shared_ptr<ResourceType>> loadedResources;
};

#include "ResourceController.inl"