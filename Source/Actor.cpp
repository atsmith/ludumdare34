// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#include "Actor.h"
#include "Map.h"

Actor::Actor( )
    : position{ this, &Actor::GetPositionConst, &Actor::GetPosition, &Actor::SetPosition },
      scale{ this, &Actor::GetScaleConst, &Actor::GetScale, &Actor::SetScale },
      rotation{ this, &Actor::GetRotationConst, &Actor::GetRotation, &Actor::SetRotation },
      boundingBox{ this, &Actor::GetBoundingBox }
{
    
}

Actor::Actor( const sf::Vector2f& position )
    : Actor{ }
{
    actorPosition = position;
}

Actor::Actor( const sf::Vector2f& position, const sf::Vector2f& scale )
    : Actor{ position }
{
    actorScale = scale;
}

Actor::Actor( const sf::Vector2f& position, const sf::Vector2f& scale, float rotation )
    : Actor{ position, scale }
{
    actorRotation = rotation;
}

Actor::Actor( const sf::Vector2f& position, const sf::Vector2f& scale, float rotation, const std::shared_ptr<sf::Texture>& texture )
    : Actor{ position, scale, rotation }
{
    SetSprite( *texture );
}

Actor::Actor( const Actor& other )
    : Actor{ other.position, other.scale, other.rotation }
{
    const sf::Texture* texture = other.sprite.getTexture( );
    if( texture )
    {
        SetSprite( *texture );
        SetSpriteRect( other.sprite.getTextureRect( ) );
    }
}

Actor& Actor::operator=( const Actor& other )
{
    actorPosition = other.position;
    actorScale = other.scale;
    actorRotation = other.rotation;
    sprite = other.sprite;

    return *this;
}

void Actor::Step( float delta_time )
{
    sprite.setPosition( position );
    sprite.setScale( scale );
    sprite.setRotation( rotation );
}

void Actor::AttachCollider( b2Body* body )
{
    collider = body;
}

void Actor::SetSprite( const sf::Texture& texture )
{
    sprite.setTexture( texture );
}

void Actor::SetSpriteRect( const sf::IntRect& rect )
{
    sprite.setTextureRect( rect );
}

sf::Sprite Actor::GetSprite( ) const
{
    return sprite;
}

void Actor::Draw( sf::RenderTarget& target, sf::RenderStates states, float opacity ) const
{
    sf::Color sprite_color = sprite.getColor( );
    sprite_color.a = ( sf::Uint8 )( 255 * opacity );
    sf::Sprite opaque_sprite = sprite;
    opaque_sprite.setColor( sprite_color );
    opaque_sprite.setPosition( position );
    opaque_sprite.setScale( scale );
    opaque_sprite.setRotation( rotation );
    target.draw( opaque_sprite, states );
}

b2Body* Actor::GetCollider( ) const
{
    return collider;
}

ActorOPCodes Actor::GetOPCode( ) const
{
    return ActorOPCodes::Actor;
}

void Actor::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    Draw( target, states, 1.f );
}

sf::Vector2f& Actor::GetPosition( )
{
    return actorPosition;
}

sf::Vector2f& Actor::GetScale( )
{
    return actorScale;
}

float& Actor::GetRotation( )
{
    return actorRotation;
}

sf::Vector2f Actor::GetPositionConst( ) const
{
    return actorPosition;
}

sf::Vector2f Actor::GetScaleConst( ) const
{
    return actorScale;
}

float Actor::GetRotationConst( ) const
{
    return actorRotation;
}

sf::Vector2f& Actor::SetPosition( const sf::Vector2f& position )
{
    actorPosition = position;
    sprite.setPosition( position );
    return actorPosition;
}

sf::Vector2f& Actor::SetScale( const sf::Vector2f& scale )
{
    actorScale = scale;
    sprite.setScale( actorScale );
    return actorScale;
}

float& Actor::SetRotation( const float& rotation )
{
    actorRotation = rotation;
    sprite.setRotation( rotation );
    return actorRotation;
}

sf::Rect<float> Actor::GetBoundingBox( ) const
{
    return sprite.getGlobalBounds( );
}