// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once

template<typename ObjectType, typename ReturnType>
class Getter
{
public:
    typedef ReturnType( ObjectType::*GetterFunc )( ) const;
    Getter( ObjectType* object, GetterFunc function );
    operator ReturnType( ) const;
    ReturnType operator()( ) const;
private:
    Getter( const Getter<ObjectType, ReturnType>& ) = delete;
    Getter<ObjectType, ReturnType>& operator=( const Getter<ObjectType, ReturnType>& ) = delete;
    Getter<ObjectType, ReturnType>& operator=( const ReturnType& ) = delete;

    ObjectType* object;
    GetterFunc function;
};

#include "Getter.inl"