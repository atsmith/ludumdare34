// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include <memory>
#include "Actor.h"
#include "SerializationOPCodes.h"

class ActorBuilder
{
public:
    static std::shared_ptr<Actor> Create( ActorOPCodes code );
    static std::shared_ptr<Actor> Create( ActorOPCodes code, const sf::Vector2f& position, const sf::Vector2f& scale, float rotation );
    static std::shared_ptr<Actor> Create( ActorOPCodes code, const std::shared_ptr<Actor>& pointer );
};