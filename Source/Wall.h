// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include "Actor.h"
#include "GetSet.h"

class Wall : public Actor
{
public:
    Wall( );
    Wall( const Wall& other );
    Wall& operator=( const Wall& other );

    GetSet<Wall, sf::Vector2f> size;
    virtual void Step( float delta_time ) override;
    virtual void Draw( sf::RenderTarget& target, sf::RenderStates states, float opacity ) const override;
    virtual ActorOPCodes GetOPCode( ) const override;
protected:
    virtual void draw( sf::RenderTarget& target, sf::RenderStates states ) const override;
private:
    void CreateCollider( );
    sf::Vector2f& GetSize( );
    sf::Vector2f GetSizeConst( ) const;
    sf::Vector2f& SetSize( const sf::Vector2f& value );

    sf::Vector2f wallSize{ 32, 32 };
    bool attachedCollider{ false };
};