// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include <string>

enum class ActorOPCodes : int
{
    Unknown = -1,
    Wall = 0,
    Tile,
    Actor,
    PlayerSpawn
};

enum class LayerOPCodes : int
{
    Unknown = -1,
    TileLayer = 0,
    ObjectLayer
};

inline ActorOPCodes FromString( const std::string& value )
{
    if( value == "wall" || value == "Wall" )
    {
        return ActorOPCodes::Wall;
    }
    if( value == "tile" || value == "Tile" )
    {
        return ActorOPCodes::Tile;
    }
    if( value == "actor" || value == "Actor" )
    {
        return ActorOPCodes::Actor;
    }
    if( value == "playerspawn" || value == "PlayerSpawn" || value == "playerSpawn" || value == "Playerspawn" )
    {
        return ActorOPCodes::PlayerSpawn;
    }

    return ActorOPCodes::Unknown;
}