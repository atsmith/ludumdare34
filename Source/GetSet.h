// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include <type_traits>

template<typename ObjectType, typename ValueType>
class GetSet
{
public:
    typedef ValueType&( ObjectType::*GetterFunc )( );
    typedef ValueType( ObjectType::*ConstGetterFunc )( ) const;
    typedef ValueType&( ObjectType::*SetterFunc )( const ValueType& );

    GetSet( ObjectType* object, ConstGetterFunc const_get_function, GetterFunc get_function, SetterFunc set_function );
    ValueType& operator=( const ValueType& value );
    operator ValueType&( );
    operator ValueType( ) const;
    ValueType& operator()( );
    ValueType operator()( ) const;
private:
    GetSet( ) = delete;
    GetSet( const GetSet<ObjectType, ValueType>& ) = delete;
    GetSet<ObjectType, ValueType>& operator=( const GetSet<ObjectType, ValueType>& ) = delete;

    ValueType Get( ) const;
    ValueType& Get( );
    ValueType& Set( const ValueType& value );

    ObjectType* object;
    GetterFunc getFunction;
    ConstGetterFunc constGetFunction;
    SetterFunc setFunction;
};

#include "GetSet.inl"