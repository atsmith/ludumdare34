// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include <memory>
#include <vector>
#include <SFML/Graphics/Rect.hpp>
#include "Actor.h"
#include "Config.h"

struct QuadTreeNode
{
    QuadTreeNode( const sf::FloatRect& bounding_box, unsigned int level );
    ~QuadTreeNode( );
    bool Split( unsigned int levels_deep, unsigned int actors_per_quad );
    bool AddActor( const std::shared_ptr<Actor>& actor, unsigned int levels_deep, unsigned int actors_per_quad );
    void GetActors( std::vector<std::shared_ptr<Actor>>& actor_list, const sf::FloatRect& view_frame ) const;
    bool IsLeaf( ) const;
    bool CheckCollision( const sf::FloatRect& view_frame, const sf::FloatRect& other ) const;
    void UpdateBounds( const sf::FloatRect& bounding_box );
    
    QuadTreeNode* upper_left;
    QuadTreeNode* upper_right;
    QuadTreeNode* lower_left;
    QuadTreeNode* lower_right;

    sf::Rect<float> bounding_box;
    unsigned int level;
    std::vector<std::shared_ptr<Actor>> actors;
};

class QuadTree
{
public:
    QuadTree( const sf::FloatRect& bounding_box, unsigned int levels_deep = 10, unsigned int actors_per_quad = 512 );
    QuadTree( const QuadTree& other );
    ~QuadTree( );
    QuadTree& operator=( const QuadTree& other );
    void Clear( );
    bool AddActor( const std::shared_ptr<Actor>& actor );
    std::vector<std::shared_ptr<Actor>> GetActors( const sf::View& looking ) const;
    void UpdateBounds( const sf::FloatRect& bounding_box );
private:
    void Clone( const QuadTree& other );
    QuadTreeNode* rootNode;
    sf::FloatRect boundingBox;
    unsigned int levelsDeep;
    unsigned int actorsPerQuad;
};