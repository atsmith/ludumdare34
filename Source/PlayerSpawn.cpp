// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#include "PlayerSpawn.h"
#include "Player.h"
#include "Map.h"

PlayerSpawn::PlayerSpawn( )
    : Actor{ },
      summonedPlayer{ false }
{

}

PlayerSpawn::PlayerSpawn( const PlayerSpawn& other )
    : Actor{ other },
      summonedPlayer{ other.summonedPlayer }
{

}

PlayerSpawn& PlayerSpawn::operator=( const PlayerSpawn& other )
{
    Actor::operator=( other );
    summonedPlayer = other.summonedPlayer;

    return *this;
}

void PlayerSpawn::Step( float delta_time )
{
    if( !summonedPlayer )
    {
        Map* map = Map::GetWorld( );
        if( !map )
        {
            return;
        }
        Player* player = Player::GetPlayerPtr( );
        player->Reset( );
        player->position = position( );
        player->rotation = rotation( );
        map->SetPlayer( player );
        summonedPlayer = true;
    }
}

ActorOPCodes PlayerSpawn::GetOPCode( ) const
{
    return ActorOPCodes::PlayerSpawn;
}