// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#include "QuadTree.h"
#include "ActorBuilder.h"

static bool operator>( const sf::Vector2f& a, const sf::Vector2f& b )
{
    bool x_same = abs( a.x - b.x ) < 0.000001f;
    bool y_same = abs( a.y - b.y ) < 0.000001f;
    return ( a.x > b.x ) || ( x_same && a.y > b.y );
}

static bool operator<( const sf::Vector2f& a, const sf::Vector2f& b )
{
    bool x_same = abs( a.x - b.x ) < 0.000001f;
    bool y_same = abs( a.y - b.y ) < 0.000001f;
    return ( a.x < b.x ) || ( x_same && a.y < b.y );
}

static bool IsInside( const sf::FloatRect& container, const sf::FloatRect& contained )
{
    return container.left < contained.left && 
        container.top < contained.top && 
        container.left + container.width > contained.left + contained.width && 
        container.top + container.height > contained.top + contained.height;
}

QuadTreeNode::QuadTreeNode( const sf::FloatRect& bounding_box, unsigned int level )
    : upper_left{ nullptr },
      upper_right{ nullptr },
      lower_left{ nullptr },
      lower_right{ nullptr },
      level{ level },
      bounding_box{ bounding_box }
{

}

QuadTreeNode::~QuadTreeNode( )
{
    actors.clear( );
    SAFE_DELETE( upper_left );
    SAFE_DELETE( upper_right );
    SAFE_DELETE( lower_left );
    SAFE_DELETE( lower_right );
}

bool QuadTreeNode::Split( unsigned int levels_deep, unsigned int actors_per_quad )
{
    if( levels_deep == level || !IsLeaf( ) )
    {
        return false;
    }

    float half_width = bounding_box.width / 2;
    float half_height = bounding_box.height / 2;
    float x = bounding_box.left;
    float y = bounding_box.top;
    std::vector<std::shared_ptr<Actor>> actor_list;

    upper_left = new QuadTreeNode{  sf::FloatRect{x, y, half_width, half_height}, level + 1 };
    upper_right = new QuadTreeNode{ sf::FloatRect{x + half_width, y, half_width, half_height}, level + 1 };
    lower_left = new QuadTreeNode{  sf::FloatRect{x, y + half_height, half_width, half_height}, level + 1 };
    lower_right = new QuadTreeNode{ sf::FloatRect{x + half_width, y + half_height, half_width, half_height}, level + 1 };

    if( IsLeaf( ) )
    {
        goto error;
    }

    return true;
error:
    SAFE_DELETE( upper_left );
    SAFE_DELETE( upper_right );
    SAFE_DELETE( lower_left );
    SAFE_DELETE( lower_right );
    return false;
}

bool QuadTreeNode::AddActor( const std::shared_ptr<Actor>& actor, unsigned int levels_deep, unsigned int actors_per_quad )
{
    if( !actor || !CheckCollision( actor->boundingBox, bounding_box ) )
    {
        return false;
    }
    auto add_with_children = [ &]( )->bool
    {
        if( upper_left->AddActor( actor, levels_deep, actors_per_quad ) )
        {
            return true;
        }
        else if( upper_right->AddActor( actor, levels_deep, actors_per_quad ) )
        {
            return true;
        }
        else if( lower_left->AddActor( actor, levels_deep, actors_per_quad ) )
        {
            return true;
        }
        else if( lower_right->AddActor( actor, levels_deep, actors_per_quad ) )
        {
            return true;
        }

        return false;
    };
    if( !IsLeaf( ) )
    {
        if( add_with_children( ) )
        {
            return true;
        }
    }
    else if( actors.size( ) == actors_per_quad )
    {
        if( !Split( levels_deep, actors_per_quad ) )
        {
            return false;
        }
        else
        {
            if( add_with_children( ) )
            {
                return true;
            }
        }
        return false;
    }

    actors.push_back( actor );
    return true;
}

void QuadTreeNode::GetActors( std::vector<std::shared_ptr<Actor>>& actor_list, const sf::FloatRect& view_frame ) const
{
    if( CheckCollision( view_frame, bounding_box ) )
    {
        for( auto actor : actors )
        {
            actor_list.push_back( actor );
           /* if( CheckCollision( view_frame, actor->boundingBox ) )
            {
                actor_list.push_back( actor );
            }*/
        }
        if( !IsLeaf( ) )
        {
            upper_left->GetActors( actor_list, view_frame );
            upper_right->GetActors( actor_list, view_frame );
            lower_left->GetActors( actor_list, view_frame );
            lower_right->GetActors( actor_list, view_frame );
        }
    }
}

void QuadTreeNode::UpdateBounds( const sf::FloatRect& bounding_box )
{
    this->bounding_box = bounding_box;
    if( !IsLeaf( ) )
    {
        float half_width = bounding_box.width / 2;
        float half_height = bounding_box.height / 2;
        float left = bounding_box.left;
        float top = bounding_box.top;
        upper_left->UpdateBounds( sf::FloatRect{ left, top, half_width, half_height } );
        upper_right->UpdateBounds( sf::FloatRect{ left + half_width, top, half_width, half_height } );
        lower_right->UpdateBounds( sf::FloatRect{ left + half_width, top + half_height, half_width, half_height } );
        lower_left->UpdateBounds( sf::FloatRect{ left, top + half_height, half_width, half_height } );
    }
}

bool QuadTreeNode::IsLeaf( ) const
{
    return !upper_left && !upper_right && !lower_left && !lower_right;
}

bool QuadTreeNode::CheckCollision( const sf::FloatRect& view_frame, const sf::FloatRect& other ) const
{
    sf::Vector2f corners[ 4 ];
    corners[ 0 ].x = view_frame.left;                    corners[ 0 ].y = view_frame.top;
    corners[ 1 ].x = view_frame.left + view_frame.width; corners[ 1 ].y = view_frame.top;
    corners[ 2 ].x = view_frame.left + view_frame.width; corners[ 2 ].y = view_frame.top + view_frame.height;
    corners[ 3 ].x = view_frame.left;                    corners[ 3 ].y = view_frame.top + view_frame.height;

    sf::Vector2f aabbcorn[ 4 ];
    aabbcorn[ 0 ].x = other.left;                      aabbcorn[ 0 ].y = other.top;
    aabbcorn[ 1 ].x = other.left + other.width;        aabbcorn[ 1 ].y = other.top;
    aabbcorn[ 2 ].x = other.left + other.width;        aabbcorn[ 2 ].y = other.top + other.height;
    aabbcorn[ 3 ].x = other.left;                      aabbcorn[ 3 ].y = other.top + other.height;

    bool inside = IsInside( view_frame, other ) || IsInside( other, view_frame );
    bool intersect = view_frame.intersects( other ) || other.intersects( view_frame );

    bool top_left  = corners[ 0 ].x <= aabbcorn[ 2 ].x && corners[ 0 ].y <= aabbcorn[ 2 ].y && 
                     corners[ 2 ].x >= aabbcorn[ 2 ].x && corners[ 2 ].y >= aabbcorn[ 2 ].y;

    bool top_right = corners[ 1 ].x >= aabbcorn[ 3 ].x && corners[ 1 ].y <= aabbcorn[ 3 ].y && 
                     corners[ 3 ].x <= aabbcorn[ 3 ].x && corners[ 3 ].y >= aabbcorn[ 3 ].y;

    bool low_right = corners[ 2 ].x >= aabbcorn[ 0 ].x && corners[ 2 ].y >= aabbcorn[ 0 ].y && 
                     corners[ 0 ].x <= aabbcorn[ 0 ].x && corners[ 0 ].y <= aabbcorn[ 0 ].y;

    bool low_left  = corners[ 3 ].x <= aabbcorn[ 1 ].x && corners[ 3 ].y >= aabbcorn[ 1 ].y && 
                     corners[ 1 ].x >= aabbcorn[ 1 ].y && corners[ 1 ].y <= aabbcorn[ 1 ].y;


    bool top    = corners[ 0 ].x >= aabbcorn[ 3 ].x && 
                  corners[ 0 ].y <= aabbcorn[ 3 ].y && 
                  corners[ 1 ].x <= aabbcorn[ 2 ].x && 
                  corners[ 1 ].y >= aabbcorn[ 0 ].y;


    bool left   = corners[ 0 ].x <= aabbcorn[ 1 ].x && 
                  corners[ 0 ].y >= aabbcorn[ 1 ].y && 
                  corners[ 3 ].y <= aabbcorn[ 3 ].y && 
                  corners[ 3 ].x >= aabbcorn[ 3 ].x;


    bool bottom = corners[ 3 ].x >= aabbcorn[ 0 ].x && 
                  corners[ 3 ].y >= aabbcorn[ 0 ].y && 
                  corners[ 2 ].x <= aabbcorn[ 2 ].x && 
                  corners[ 2 ].y <= aabbcorn[ 2 ].y;


    bool right  = corners[ 1 ].x >= aabbcorn[ 0 ].x && 
                  corners[ 1 ].y >= aabbcorn[ 0 ].y && 
                  corners[ 2 ].x <= aabbcorn[ 2 ].x && 
                  corners[ 2 ].y <= aabbcorn[ 2 ].y;

    bool vertical = corners[ 0 ].x <= aabbcorn[ 0 ].x &&
        corners[ 0 ].y >= aabbcorn[ 0 ].y &&
        corners[ 1 ].x >= aabbcorn[ 1 ].x &&
        corners[ 2 ].y <= aabbcorn[ 2 ].y;

    bool horiz = corners[ 0 ].x >= aabbcorn[ 0 ].x &&
        corners[ 0 ].y <= aabbcorn[ 0 ].y &&
        corners[ 2 ].x <= aabbcorn[ 2 ].x &&
        corners[ 2 ].y <= aabbcorn[ 2 ].y;



    return inside || intersect || top_left || top_right || low_right || low_left || top || left || bottom || right || vertical || horiz;
}

QuadTree::QuadTree( const sf::FloatRect& bounding_box, unsigned int levels_deep, unsigned int actors_per_quad )
    : levelsDeep{ levels_deep },
      actorsPerQuad{ actors_per_quad },
      boundingBox{ bounding_box },
      rootNode{ new QuadTreeNode{ bounding_box, 1 } }
{

}

QuadTree::QuadTree( const QuadTree& other )
    : QuadTree{ other.boundingBox, other.levelsDeep, other.actorsPerQuad }
{
    Clone( other );
}

QuadTree::~QuadTree( )
{
    SAFE_DELETE( rootNode );
}

QuadTree& QuadTree::operator=( const QuadTree& other )
{
    Clear( );
    Clone( other );

    return *this;
}

void QuadTree::Clear( )
{
    if( !rootNode )
    {
        return;
    }
    rootNode->actors.clear( );
    SAFE_DELETE( rootNode->upper_left );
    SAFE_DELETE( rootNode->upper_right );
    SAFE_DELETE( rootNode->lower_left );
    SAFE_DELETE( rootNode->lower_right );
}

bool QuadTree::AddActor( const std::shared_ptr<Actor>& actor )
{
    if( !rootNode )
    {
        return false;
    }
    return rootNode->AddActor( actor, levelsDeep, actorsPerQuad );
}

std::vector<std::shared_ptr<Actor>> QuadTree::GetActors( const sf::View& looking ) const
{
    std::vector<std::shared_ptr<Actor>> actor_list;
    sf::FloatRect view_frame;
    if( !rootNode )
    {
        goto error;
    }
    view_frame.left = looking.getCenter( ).x - looking.getSize( ).x;
    view_frame.top = looking.getCenter( ).y - looking.getSize( ).y;
    view_frame.width = looking.getSize( ).x * 2;
    view_frame.height = looking.getSize( ).y * 2;

    rootNode->GetActors( actor_list, view_frame );
    // fall through
error:
    return actor_list;
}

void QuadTree::UpdateBounds( const sf::FloatRect& bounding_box )
{
    boundingBox = bounding_box;
    rootNode->UpdateBounds( bounding_box );
}

void QuadTree::Clone( const QuadTree& other )
{
    levelsDeep = other.levelsDeep;
    actorsPerQuad = other.actorsPerQuad;
    boundingBox = other.boundingBox;
    rootNode->bounding_box = boundingBox;
    sf::View view;
    view.setCenter( boundingBox.left + boundingBox.width / 2, boundingBox.top + boundingBox.height / 2 );
    view.setSize( boundingBox.width, boundingBox.height );
    auto actors = other.GetActors( view );
    for( auto actor : actors )
    {
        AddActor( ActorBuilder::Create( actor->GetOPCode( ), actor ) );
    }
}