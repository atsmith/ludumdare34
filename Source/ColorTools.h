// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include "Config.h"
#include "Globals.h"
#include <SFML/Graphics/Color.hpp>

inline sf::Color ColorFromUInt( unsigned int color_value )
{
    sf::Color color;
    color.a = ( color_value & alpha_mask );
    color.b = ( color_value & blue_mask ) >> 8;
    color.g = ( color_value & green_mask ) >> 16;
    color.r = ( color_value & red_mask ) >> 24;

    return color;
}