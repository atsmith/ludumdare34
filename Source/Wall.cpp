// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#include <SFML/Graphics/RectangleShape.hpp>
#include <Box2D/Collision/Shapes/b2PolygonShape.h>
#include "Map.h"
#include "Wall.h"

Wall::Wall( )
    : size{ this, &Wall::GetSizeConst, &Wall::GetSize, &Wall::SetSize }
{
    
}

Wall::Wall( const Wall& other )
    : Wall{ }
{
    wallSize = other.size;
}

Wall& Wall::operator=( const Wall& other )
{
    wallSize = other.size;
    attachedCollider = other.attachedCollider;
    return *this;
}

void Wall::Step( float delta_time )
{
    Actor::Step( delta_time );
    if( !attachedCollider )
    {
        CreateCollider( );
    }
}

void Wall::Draw( sf::RenderTarget& target, sf::RenderStates states, float opacity ) const
{
    sf::RectangleShape drawing_shape{ sf::Vector2f{ wallSize.x, wallSize.y } };
    drawing_shape.setPosition( position );
    sf::Color fill_color = sf::Color::Red;
    fill_color.a = ( sf::Uint8 )( 255 * opacity );
    drawing_shape.setFillColor( fill_color );

    target.draw( drawing_shape, states );
}

ActorOPCodes Wall::GetOPCode( ) const
{
    return ActorOPCodes::Wall;
}

void Wall::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    Draw( target, states, 1.f );
}

sf::Vector2f& Wall::GetSize( )
{
    return wallSize;
}

sf::Vector2f Wall::GetSizeConst( ) const
{
    return wallSize;
}

sf::Vector2f& Wall::SetSize( const sf::Vector2f& value )
{
    wallSize = value;
    return wallSize;
}

void Wall::CreateCollider( )
{
    b2BodyDef body_definition;
    body_definition.type = b2_staticBody;
    body_definition.position.Set( position( ).x, position( ).y );
    b2Body* body = Map::GetWorld( )->CreateBody( &body_definition );

    b2PolygonShape bounding_box_shape;
    bounding_box_shape.SetAsBox( wallSize.x, wallSize.y );
    body->CreateFixture( &bounding_box_shape, 0.0f );

    AttachCollider( body );

    attachedCollider = true;
}