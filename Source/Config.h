// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once

#define GLOBAL_VAR static

#define SAFE_DELETE(x) if(x){delete x; x = nullptr;}