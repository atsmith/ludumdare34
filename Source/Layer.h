// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include <memory>
#include <vector>
#include <SFML/Graphics/Drawable.hpp>
#include "Actor.h"
#include "QuadTree.h"
#include "Getter.h"

class Layer : public sf::Drawable
{
public:
    Layer( const sf::FloatRect& bounding_box );
    Layer( const Layer& other );
    virtual ~Layer( ) = default;
    Layer& operator=( const Layer& other );
    virtual bool AddActor( const std::shared_ptr<Actor>& actor );
    virtual void Step( float delta_time );
    virtual bool IsStaticLayer( ) const;
    void SetVisible( bool visible );
    void SetOpacity( float opacity );
    sf::FloatRect GetBoundingBox( ) const;
    virtual std::shared_ptr<Layer> Clone( ) const;
protected:
    virtual void draw( sf::RenderTarget& target, sf::RenderStates states ) const override;
    QuadTree actors;
    float opacity;
    bool visible;
    sf::FloatRect boundingBox;
};

class ActiveLayer : public Layer
{
public:
    ActiveLayer( const sf::FloatRect& bounding_box );
    ActiveLayer( const ActiveLayer& other );
    ~ActiveLayer( ) = default;
    ActiveLayer& operator=( const ActiveLayer& other );
    virtual bool AddActor( const std::shared_ptr<Actor>& actor ) override;
    virtual void Step( float delta_time ) override;
    virtual bool IsStaticLayer( ) const override;
    virtual std::shared_ptr<Layer> Clone( ) const override;
protected:
    std::vector<std::shared_ptr<Actor>> actorList;
private:
    void Clone( const ActiveLayer& other );
};