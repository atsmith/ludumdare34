// Copyright (c) 2015 InfiniRare Games.  All rights reserved.

template<typename ObjectType, typename ValueType>
GetSet<ObjectType, ValueType>::GetSet( ObjectType* object, typename GetSet::ConstGetterFunc const_get_function, typename GetSet::GetterFunc get_function, typename GetSet::SetterFunc set_function )
    : object{ object },
      getFunction{ get_function },
      constGetFunction{ const_get_function },
      setFunction{ set_function }
{

}

template<typename ObjectType, typename ValueType>
ValueType& GetSet<ObjectType, ValueType>::operator=( const ValueType& value )
{
    return Set( value );
}

template<typename ObjectType, typename ValueType>
GetSet<ObjectType, ValueType>::operator ValueType&( )
{
    return Get( );
}

template<typename ObjectType, typename ValueType>
GetSet<ObjectType, ValueType>::operator ValueType( ) const
{
    return Get( );
}

template<typename ObjectType, typename ValueType>
ValueType& GetSet<ObjectType, ValueType>::operator()( )
{
    return Get( );
}

template<typename ObjectType, typename ValueType>
ValueType GetSet<ObjectType, ValueType>::operator()( ) const
{
    return Get( );
}

template<typename ObjectType, typename ValueType>
ValueType GetSet<ObjectType, ValueType>::Get( ) const
{
    return (object->*constGetFunction)( );
}

template<typename ObjectType, typename ValueType>
ValueType& GetSet<ObjectType, ValueType>::Get( )
{
    return (object->*getFunction)( );
}

template<typename ObjectType, typename ValueType>
ValueType& GetSet<ObjectType, ValueType>::Set( const ValueType& value )
{
    return (object->*setFunction)( value );
}