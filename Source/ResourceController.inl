// Copyright (c) 2015 InfiniRare Games.  All rights reserved.

template<typename ResourceType>
ResourceController<ResourceType>::ResourceController( const sf::String& data_location )
    : dataLocation{ data_location }
{
    if( directory_separator != dataLocation.substring( dataLocation.getSize( ) - 1, 1 ) )
    {
        dataLocation.insert( dataLocation.getSize( ), directory_separator );
    }
}

template<typename ResourceType>
bool ResourceController<ResourceType>::IsResourceLoaded( const sf::String& resource_name ) const
{
    return 1 == loadedResources.count( resource_name );
}

template<typename ResourceType>
bool ResourceController<ResourceType>::LoadResource( const sf::String& resource_name )
{
    std::shared_ptr<ResourceType> loaded_resource{ new ResourceType };
    if( !loaded_resource )
    {
        return false;
    }

    if( !loaded_resource->loadFromFile( dataLocation + resource_name ) )
    {
        return false;
    }

    auto success = loadedResources.insert( std::make_pair( resource_name, loaded_resource ) );

    return success.second;
}

template<typename ResourceType>
template<typename OtherResource>
bool ResourceController<ResourceType>::LoadResource( const sf::String& resource_name, const OtherResource& resource )
{
    return LoadResource( resource_name );
}

template<>
template<>
inline bool ResourceController<sf::Texture>::LoadResource<sf::Image>( const sf::String& resource_name, const sf::Image& resource )
{
    std::shared_ptr<sf::Texture> loaded_resource{ new sf::Texture };
    if( !loaded_resource )
    {
        return false;
    }

    if( !loaded_resource->loadFromImage( resource ) )
    {
        return false;
    }

    auto success = loadedResources.insert( std::make_pair( resource_name, loaded_resource ) );

    return success.second;
}

template<typename ResourceType>
std::shared_ptr<ResourceType> ResourceController<ResourceType>::GetResourcePtr( const sf::String& resource_name )
{
    auto found = loadedResources.find( resource_name );
    if( loadedResources.end( ) == found )
    {
        return std::shared_ptr<ResourceType>{};
    }

    return found->second;
}

template<typename ResourceType>
ResourceType& ResourceController<ResourceType>::GetResource( const sf::String& resource_name )
{
    auto found = loadedResources.find( resource_name );
    if( loadedResources.end( ) == found )
    {
        // return temp variable because we have no logging...
#pragma warning(push)
#pragma warning(disable : 4172)
        return ResourceType{ };
#pragma warning(pop)
    }
    
    return *found->second;
}