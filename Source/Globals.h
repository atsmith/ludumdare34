// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include <SFML/Graphics/Image.hpp>
#include <SFML/Graphics/Texture.hpp>
#include "Config.h"
#include "ResourceController.h"
#include "Map.h"

GLOBAL_VAR ResourceController<sf::Image> image_controller{ "Data/Textures/" };
GLOBAL_VAR ResourceController<sf::Texture> texture_controller{ "Data/Textures/" };
GLOBAL_VAR ResourceController<Map> map_controller{ "Data/Maps/" };

GLOBAL_VAR const unsigned int red_mask{ 0xFF000000 };
GLOBAL_VAR const unsigned int green_mask{ 0x00FF0000 };
GLOBAL_VAR const unsigned int blue_mask{ 0x0000FF00 };
GLOBAL_VAR const unsigned int alpha_mask{ 0x000000FF };