// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#include <Windows.h>
#include "ResourceController.h"
#include <SFML/Graphics.hpp>

int CALLBACK WinMain( HINSTANCE, HINSTANCE, LPSTR, int)
{
    sf::RenderWindow window{ sf::VideoMode{ 800, 600 }, "Ludum Dare 34" };
    ResourceController<sf::Texture> texture_controller{ "Data/Textures" };

    if( !texture_controller.LoadResource( "wall.png" ) )
    {
        return EXIT_FAILURE;
    }

    sf::Sprite sprite{ texture_controller.GetResource( "wall.png" ) };

    while( window.isOpen( ) )
    {
        sf::Event event;
        while( window.pollEvent( event ) )
        {
            if( sf::Event::Closed == event.type )
            {
                window.close( );
            }
        }

        window.clear( );

        window.draw( sprite );

        window.display( );
    }

    return EXIT_SUCCESS;
}