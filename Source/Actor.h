// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <Box2D/Dynamics/b2Body.h>
#include "GetSet.h"
#include "Getter.h"
#include "SerializationOPCodes.h"

class Actor : public sf::Drawable
{
public:
    Actor( );
    Actor( const sf::Vector2f& position );
    Actor( const sf::Vector2f& position, const sf::Vector2f& scale );
    Actor( const sf::Vector2f& position, const sf::Vector2f& scale, float rotation );
    Actor( const sf::Vector2f& position, const sf::Vector2f& scale, float rotation, const std::shared_ptr<sf::Texture>& texture );
    Actor( const Actor& other );
    virtual ~Actor( ) = default;

    Actor& operator=( const Actor& other );

    virtual void Step( float delta_time );

    void AttachCollider( b2Body* body );

    void SetSprite( const sf::Texture& texture );
    void SetSpriteRect( const sf::IntRect& rect );
    sf::Sprite GetSprite( ) const;

    GetSet<Actor, sf::Vector2f> position;
    GetSet<Actor, sf::Vector2f> scale;
    GetSet<Actor, float> rotation;

    Getter<Actor, sf::Rect<float>> boundingBox;
    virtual void Draw( sf::RenderTarget& target, sf::RenderStates states, float opacity ) const;
    b2Body* GetCollider( ) const;
    virtual ActorOPCodes GetOPCode( ) const;
protected:
    virtual void draw( sf::RenderTarget& target, sf::RenderStates states ) const override;
private:

    sf::Vector2f GetPositionConst( ) const;
    sf::Vector2f GetScaleConst( ) const;
    float GetRotationConst( ) const;
    sf::Vector2f& GetPosition( );
    sf::Vector2f& GetScale( );
    float& GetRotation( );
    sf::Vector2f& SetPosition( const sf::Vector2f& position );
    sf::Vector2f& SetScale( const sf::Vector2f& scale );
    float& SetRotation( const float& rotation );

    sf::Rect<float> GetBoundingBox( ) const;

    sf::Vector2f actorPosition{ 0, 0 };
    sf::Vector2f actorScale{ 1, 1 };
    float actorRotation{ 0.0f };
    sf::Sprite sprite{ };
    b2Body* collider{ nullptr };
};