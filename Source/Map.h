// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include <memory>
#include <vector>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/System/String.hpp>
#include <Box2D/Dynamics/b2World.h>
#include "Actor.h"
#include "Layer.h"
#include "Getter.h"
#include "Player.h"

class Map : public sf::Drawable
{
public:
    static Map* GetWorld( );

    Map( );
    Map( const Map& other );
    Map& operator= ( const Map& other );

    bool loadFromFile( const sf::String& file_name );
    void Step( float delta_time );
    Getter<Map, sf::Color> background;
    bool AddActor( const std::shared_ptr<Actor>& actor, bool is_active = true );
    void SetPlayer( Player* player );
    b2Body* CreateBody( b2BodyDef* body_definition );
protected:
    virtual void draw( sf::RenderTarget& target, sf::RenderStates states ) const override;
private:
    void Clone( const Map& other );
    sf::Color GetBackgroundColor( ) const;
    sf::Vector2u mapSize;
    sf::Vector2i tileSize;
    sf::Color backgroundColor;
    std::vector<std::shared_ptr<Layer>> layers;
    Player* player;
    b2World physicsWorld;
};