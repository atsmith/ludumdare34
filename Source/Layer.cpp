// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#include "Layer.h"

Layer::Layer( const sf::FloatRect& bounding_box )
    : actors{ bounding_box },
      boundingBox{ bounding_box },
      opacity{ 1.f },
      visible{ true }
{

}

Layer::Layer( const Layer& other )
    : actors{ other.actors },
      boundingBox{ other.boundingBox },
      opacity{ other.opacity },
      visible{ other.visible }
{
    
}

Layer& Layer::operator=( const Layer& other )
{
    boundingBox = other.boundingBox;
    actors = other.actors;
    actors.UpdateBounds( boundingBox );
    opacity = other.opacity;
    visible = other.visible;
    return *this;
}

bool Layer::AddActor( const std::shared_ptr<Actor>& actor )
{
    return actors.AddActor( actor );
}

void Layer::SetVisible( bool visible )
{
    this->visible = visible;
}

void Layer::SetOpacity( float opacity )
{
    this->opacity = opacity;
}

sf::FloatRect Layer::GetBoundingBox( ) const
{
    return boundingBox;
}

void Layer::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    if( !visible )
    {
        return;
    }
    std::vector<std::shared_ptr<Actor>> actor_list;
    actor_list = actors.GetActors( target.getView( ) );
    for( std::shared_ptr<Actor> actor : actor_list )
    {
        target.draw( *actor, states );
    }
}

void Layer::Step( float delta_time )
{

}

bool Layer::IsStaticLayer( ) const
{
    return true;
}

std::shared_ptr<Layer> Layer::Clone( ) const
{
    std::shared_ptr<Layer> layer{ new Layer{ *this } };
    return layer;
}

ActiveLayer::ActiveLayer( const sf::FloatRect& bounding_box )
    : Layer{ bounding_box }
{

}

ActiveLayer::ActiveLayer( const ActiveLayer& other )
    : Layer{ other }
{
    Clone( other );
}

ActiveLayer& ActiveLayer::operator=( const ActiveLayer& other )
{
    Clone( other );
    return *this;
}

bool ActiveLayer::AddActor( const std::shared_ptr<Actor>& actor )
{
    if( Layer::AddActor( actor ) )
    {
        actorList.push_back( actor );
        return true;
    }
    return false;
}

void ActiveLayer::Step( float delta_time )
{
    for( std::shared_ptr<Actor> actor : actorList )
    {
        actor->Step( delta_time );
    }
}

bool ActiveLayer::IsStaticLayer( ) const
{
    return false;
}

void ActiveLayer::Clone( const ActiveLayer& other )
{
    Layer::operator=( other );
    sf::View view;
    view.setCenter( boundingBox.left + boundingBox.width, boundingBox.top + boundingBox.height );
    view.setSize( boundingBox.width * 2, boundingBox.height * 2 );
    auto actors = Layer::actors.GetActors( view );
    for( auto actor : actors )
    {
        actorList.push_back( actor );
    }
}

std::shared_ptr<Layer> ActiveLayer::Clone( ) const
{
    std::shared_ptr<Layer> layer{ new ActiveLayer{ *this } };
    return layer;
}