// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include <SFML/Window/Mouse.hpp>
#include "Actor.h"

class Player : public Actor
{
public:
    static Player* GetPlayerPtr( );

    Player( );
    void SetRequiredFood( int required_food );
    void SetMoveSpeed( float move_speed );

    void OnMouseMove( const sf::Vector2f& new_position );
    void OnMouseUp( sf::Mouse::Button button_up );

    virtual void Step( float delta_time ) override;

    void GatherFood( );

    void Reset( );
protected:
    virtual void draw( sf::RenderTarget& target, sf::RenderStates states ) const override;
private:
    void CreateCollider( );
    int babyFoodGathered;
    int babyFoodNeedToGather;
    bool holdingBaby;
    float moveSpeed{ 30.f };
    sf::Vector2f targetPosition{ };

    bool hasCollider{ false };
};