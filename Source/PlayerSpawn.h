// Copyright (c) 2015 InfiniRare Games.  All rights reserved.
#pragma once
#include "Actor.h"

class PlayerSpawn : public Actor
{
public:
    PlayerSpawn( );
    PlayerSpawn( const PlayerSpawn& other );
    PlayerSpawn& operator=( const PlayerSpawn& other );
    virtual void Step( float delta_time ) override;
    virtual ActorOPCodes GetOPCode( ) const override;
private:
    bool summonedPlayer;
};