// Copyright (c) 2015 InfiniRare Games.  All rights reserved.

template<typename ObjectType, typename ReturnType>
Getter<ObjectType, ReturnType>::Getter( ObjectType* object, typename Getter<ObjectType, ReturnType>::GetterFunc function )
    : object{ object },
      function{ function }
{

}

template<typename ObjectType, typename ReturnType>
Getter<ObjectType, ReturnType>::operator ReturnType( ) const
{
    return (object->*function)( );
}

template<typename ObjectType, typename ReturnType>
ReturnType Getter<ObjectType, ReturnType>::operator()( ) const
{
    return (object->*function)( );
}