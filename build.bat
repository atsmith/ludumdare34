@SETLOCAL
@ECHO OFF

@REM This file will build the dependencies for the game
@REM This should be called once when first developing
@REM and any time there is a change to one of the files
@REM in one of the third party libraries.

if not exist Bin mkdir Bin

pushd Bin

if not exist SFMLBuild mkdir SFMLBuild
if not exist tinyxmlBuild mkdir tinyxmlBuild
if not exist Box2DBuild mkdir Box2DBuild

pushd SFMLBuild
@REM Build SFML Debug and Release versions
cmake -DCMAKE_INSTALL_PREFIX="../../" -DCMAKE_DEBUG_POSTFIX="_Debug" ../../ThirdParty/SFML-2.3.2 -G "Visual Studio 12 Win64" >NUL 2>&1

devenv SFML.sln /Build "Debug" /project INSTALL > NUL 2>&1
devenv SFML.sln /Build "Release" /project INSTALL > NUL 2>&1

@REM Get ouf of SFMLBuild
popd

pushd tinyxmlBuild
@REM Build tinyxml2 Debug and Release versions
cmake -DCMAKE_INSTALL_BINDIR="Bin" -DCMAKE_INSTALL_LIBDIR="Lib" -DCMAKE_INSTALL_INCLUDEDIR="Include/tinyxml" -DBUILD_SHARED_LIBS=TRUE -DBUILD_STATIC_LIBS=FALSE -DCMAKE_INSTALL_PREFIX="../../" -DCMAKE_DEBUG_POSTFIX="_Debug" ../../ThirdParty/tinyxml2 -G "Visual Studio 12 Win64" >NUL 2>&1

devenv tinyxml2.sln /Build "Debug" /project INSTALL >NUL 2>&1
devenv tinyxml2.sln /Build "Release" /project INSTALL >NUL 2>&1

@REM Get out of tinyxmlBuild
popd

pushd Box2DBuild
cmake -DCMAKE_INSTALL_PREFIX="../../" -DBOX2D_BUILD_SHARED=FALSE -DBOX2D_BUILD_STATIC=TRUE -DBOX2D_INSTALL=TRUE -DBOX2D_BUILD_EXAMPLES=FALSE -DCMAKE_DEBUG_POSTFIX="_Debug" ../../ThirdParty/Box2D/Box2D -G "Visual Studio 12 Win64" >NUL 2>&1

devenv Box2D.sln /Build "Debug" /project INSTALL >NUL 2>&1
devenv Box2D.sln /Build "Release" /project INSTALL >NUL 2>&1

@REM Get out of Box2DBuild
popd

@REM Get out of Bin
popd

@REM SFML creates these in the default directory, we don't need them.
if exist cmake RMDIR /S /Q cmake
if exist license.txt del license.txt
if exist readme.txt del readme.txt

@ECHO ON
@ENDLOCAL
